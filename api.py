from flask import Flask
from flask_restful import Api, Resource, reqparse

app = Flask(__name__)
api = Api(app)


@app.route("/")
class Admin(Resource):
    # checks authentication of the admin
    def post(self, username, password):
        for admin in admins:
            if username == admin["username"] and password == admin["password"]:
                return "Welcome {} ))".format(admin["username"]), 200
        return "Your account or password is incorrect", 404


@app.route("/")
class Worker(Resource):
    def get(self, id):
        return workers, 200

    # adds an employee if there is no worker with id
    def post(self, id):
        parser = reqparse.RequestParser()
        parser.add_argument("fname")
        parser.add_argument("lname")
        parser.add_argument("salary")
        args = parser.parse_args()

        for worker in workers:
            if (id == worker["id"]):
                return "Worker with id {} already exists".format(id), 400

        worker = {
            "id": id,
            "fname": args["fname"],
            "lname": args["lname"],
            "salary": int(args["salary"])
        }
        workers.append(worker)
        updateJsonFile(worker_json, workers)
        return "you successfully added worker", 201

    # changes the employee's salary
    def put(self, id):
        parser = reqparse.RequestParser()
        parser.add_argument("salary")
        args = parser.parse_args()

        for worker in workers:
            if (id == worker["id"]):
                worker["salary"] = int(args["salary"])
                updateJsonFile(worker_json, workers)
                return "you successfully edited worker salary", 201
        return "Worker with id {} is not exists".format(id), 400

    def delete(self, id):
        for i in range(len(workers)):
            if workers[i]["id"] == id:
                workers.pop(i)
                updateJsonFile(worker_json, workers)
                return "{} is deleted.".format(id), 200
        return "Worker with id {} is not exists".format(id), 400


@app.route("/")
class Product(Resource):

    def get(self, id=0):
        if id == 0:
            return products, 200
        else:
            for product in products:
                if product["id"] == id:
                    return "{} {}".format(product["name"], product["price"]), 200
            return "Product with id {} isn't exists".format(id), 400

    # adds a product
    def post(self, id):
        parser = reqparse.RequestParser()
        parser.add_argument("name")
        parser.add_argument("price")
        parser.add_argument("quantity")
        args = parser.parse_args()

        for product in products:
            if (id == product["id"]):
                return "Product with id {} already exists".format(id), 400

        product = {
            "id": id,
            "name": args["name"],
            "price": int(args["price"]),
            "quantity": int(args["quantity"])
        }
        products.append(product)
        updateJsonFile(product_json, products)
        return "you successfully added product", 201

    # changes the product's price
    def put(self, id):
        parser = reqparse.RequestParser()
        parser.add_argument("price")
        args = parser.parse_args()

        for product in products:
            if (id == product["id"]):
                product["price"] = int(args["price"])
                updateJsonFile(product_json, products)
                return "you successfully edited product price", 201
        return "Product with id {} is not exists".format(id), 400

    def delete(self, id):
        for i in range(len(products)):
            if products[i]["id"] == id:
                products.pop(i)
                updateJsonFile(product_json, products)
                return "{} is deleted.".format(id), 200
        return "Product with id {} is not exists".format(id), 400

    @classmethod
    def decProductQuantity(self, id, quantity):
        for i in range(len(products)):
            if products[i]["id"] == id:
                if quantity < products[i]["quantity"]:
                    products[i]["quantity"] -= quantity
                    return True
                if quantity == products[i]["quantity"]:
                    products.pop(i)
                    return True
                print("There are not {} products".format(quantity))
                return False

    @classmethod
    def incProductQuantity(self, id, quantity):
        for product in products:
            if product["id"] == id:
                product["quantity"] += int(quantity)
                return True
        return False


@app.route("/")
class BasketProduct(Resource):
    def get(self, id):
        return basketProducts, 200

    # adds a product
    def post(self, id):
        parser = reqparse.RequestParser()
        parser.add_argument("name")
        parser.add_argument("price")
        parser.add_argument("quantity")
        args = parser.parse_args()

        for product in basketProducts:
            if (id == product["id"]):
                if Product.decProductQuantity(id, int(args["quantity"])):
                    product["quantity"] += int(args["quantity"])
                    updateJsonFile(basket_json, basketProducts)
                    updateJsonFile(product_json, products)
                    return "you successfully add {} to your basket".format(self.name)
                return "isn't enough product", 404

        if Product.decProductQuantity(id, int(args["quantity"])):
            basketProducts.append(
                {'id': id, 'name': args["name"], 'quantity': int(args["quantity"]), 'price': int(args["price"])})

            updateJsonFile(basket_json, basketProducts)
            updateJsonFile(product_json, products)

            return "you successfully added product", 201
        return "isn't enough product", 404

    # changes the product's quantity
    def put(self, id):
        parser = reqparse.RequestParser()
        parser.add_argument("quantity")
        args = parser.parse_args()

        for product in basketProducts:
            if (id == product["id"]):
                product["quantity"] = int(args["quantity"])
                updateJsonFile(basket_json, basketProducts)
                return "you successfully edited product quantity", 201
        return "Product with id {} is not exists".format(id), 400

    def delete(self, id):
        for i in range(len(basketProducts)):
            if basketProducts[i]["id"] == id:
                Product.incProductQuantity(id, basketProducts[i]["quantity"])
                basketProducts.pop(i)
                updateJsonFile(basket_json, basketProducts)
                updateJsonFile(product_json, products)
                return "you successfully removed product from basket", 200

        for i in range(len(products)):
            if basketProducts[i]["id"] == id:
                product = Product(id, basketProducts[i]["name"], basketProducts[i]["price"],
                                  basketProducts[i]["quantity"])
                Product.incProductQuantity(product)
                basketProducts.pop(i)

                updateJsonFile(basket_json, basketProducts)
                updateJsonFile(product_json, products)
                return "{} is deleted.".format(id), 200
            return "Product with id {} is not exists".format(id), 400


import json
import os

admin_json = os.path.join(os.path.expanduser('~'), 'PycharmProjects', 'login', 'json_files', 'admin')
with open(admin_json, 'r') as f:
    admins = json.load(f)

worker_json = os.path.join(os.path.expanduser('~'), 'PycharmProjects', 'login', 'json_files', 'worker')
with open(worker_json, 'r') as f:
    workers = json.load(f)

product_json = os.path.join(os.path.expanduser('~'), 'PycharmProjects', 'login', 'json_files', 'shop')
with open(product_json, 'r') as f:
    products = json.load(f)

basket_json = os.path.join(os.path.expanduser('~'), 'PycharmProjects', 'login', 'json_files', 'basket')
with open(basket_json, 'r') as f:
    basketProducts = json.load(f)


def updateJsonFile(filename, _list):
    os.remove(filename)
    with open(filename, 'w') as f:
        json.dump(_list, f, indent=4)


api.add_resource(Admin, "/admin/<string:username>/<string:password>")
api.add_resource(Worker, "/worker/<int:id>")
api.add_resource(Product, "/product/<int:id>")
api.add_resource(BasketProduct, "/basketProduct/<int:id>")

app.run(host='127.0.0.1', port=5000, debug=True)
