This repository contains Rest Api, that is used for the following parts. 

- Administrator panel

     - Admin authentication of the Shop project
     
     - Add and remove workers
     
     - Changes worker's salary
     
     - Add and remove products of the shop
     
     - Changes product's price
     
     - Accountancy
     
- Shoping
    
     - Buy product
     
     - Changes product's quantity
     
     - Remove product



